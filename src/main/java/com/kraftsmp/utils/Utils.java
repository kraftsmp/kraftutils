package com.kraftsmp.utils;

import java.util.concurrent.TimeUnit;

public class Utils {

    public static long ticksToTime(int ticks, TimeUnit unit) {
        int milliseconds = ticks * 50;
        return TimeUnit.MILLISECONDS.convert(milliseconds, unit);
    }
}